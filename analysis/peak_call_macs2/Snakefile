genotype = ['WT', 'KO']


rule all:
	input:
		expand('samples/{genotype}_keepdup_macs2_norm_WT_input_anno_peaks.txt', genotype=genotype),
		'samples/WT_keepdup_macs2_norm_KO_anno_peaks.txt',
		'samples/KO_keepdup_norm_WT_peaks.narrowPeak'

rule macs2_keep_duplicates:
	input:
		antiGPS2_dup = 'samples/male_mix_anti-GPS2_{condition}_M_trimmed_filtered.sorted.bam',
		chip_input_dup = 'samples/male_mix_input_WT_M_trimmed_filtered.sorted.bam'
	output:
		peaks_xls_dup = 'samples/{condition}_keepdup_norm_WT_input_peaks.xls',
		narrowpeaks_dup = 'samples/{condition}_keepdup_norm_WT_input_peaks.narrowPeak',
		rscript_dup = 'samples/{condition}_keepdup_norm_WT_input_model.r'
	params:
		prefix_dup = '{condition}_keepdup_norm_WT_input',
		dir = 'samples/'
	shell:
		'source activate py27 && '
		'macs2 callpeak -t {input.antiGPS2_dup} -c {input.chip_input_dup} -f BAM -g mm -n {params.prefix_dup} -B --outdir {params.dir} --keep-dup all'

rule macs2_keep_duplicates_ko_control:
	input:
		antiGPS2_dup = 'samples/male_mix_anti-GPS2_WT_M_trimmed_filtered.sorted.bam',
		antiGPS2_dup_ko = 'samples/male_mix_anti-GPS2_KO_M_trimmed_filtered.sorted.bam'
	output:
		peaks_xls_dup = 'samples/WT_keepdup_norm_KO_peaks.xls',
		narrowpeaks_dup = 'samples/WT_keepdup_norm_KO_peaks.narrowPeak',
		rscript_dup = 'samples/WT_keepdup_norm_KO_model.r'
	params:
		prefix_dup = 'WT_keepdup_norm_KO',
		dir = 'samples/'
	shell:
		'source activate py27 && '
		'macs2 callpeak -t {input.antiGPS2_dup} -c {input.antiGPS2_dup_ko} -f BAM -g mm -n {params.prefix_dup} -B --outdir {params.dir} --keep-dup all'

rule macs2_keep_duplicates_wt_control:
	input:
		antiGPS2_dup = 'samples/male_mix_anti-GPS2_WT_M_trimmed_filtered.sorted.bam',
		antiGPS2_dup_ko = 'samples/male_mix_anti-GPS2_KO_M_trimmed_filtered.sorted.bam'
	output:
		peaks_xls_dup = 'samples/KO_keepdup_norm_WT_peaks.xls',
		narrowpeaks_dup = 'samples/KO_keepdup_norm_WT_peaks.narrowPeak',
		rscript_dup = 'samples/KO_keepdup_norm_WT_model.r'
	params:
		prefix_dup = 'KO_keepdup_norm_WT',
		dir = 'samples/'
	shell:
		'source activate py27 && '
		'macs2 callpeak -t {input.antiGPS2_dup_ko} -c {input.antiGPS2_dup} -f BAM -g mm -n {params.prefix_dup} -B --outdir {params.dir} --keep-dup all'

rule annotate_peaks:
	input:
		narrow_peaks = 'samples/{condition}_keepdup_norm_WT_input_peaks.narrowPeak'
	output:
		anno_peaks = 'samples/{condition}_keepdup_macs2_norm_WT_input_anno_peaks.txt',
		go_analysis = 'samples/go_analysis_{condition}_macs2_peaks/'
	params:
		go_dir = 'samples/go_analysis_{condition}_keepdup_norm_WT_input_macs2_peaks/'
	shell:
		'annotatePeaks.pl {input.narrow_peaks} mm10 > {output.anno_peaks} -go {params.go_dir}'

rule annotate_peaks_ko:
	input:
		narrow_peaks = 'samples/WT_keepdup_norm_KO_peaks.narrowPeak'
	output:
		anno_peaks = 'samples/WT_keepdup_macs2_norm_KO_anno_peaks.txt',
		go_analysis = 'samples/go_analysis_WT_keepdup_norm_KO_macs2_peaks/'
	params:
		go_dir = 'samples/go_analysis_WT_keepdup_norm_KO_macs2_peaks/'
	shell:
		'annotatePeaks.pl {input.narrow_peaks} mm10 > {output.anno_peaks} -go {params.go_dir}'


