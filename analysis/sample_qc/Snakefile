genotype = ['WT', 'KO']
sampletype = ['anti-GPS2', 'input']
trimmomatic_jar = '../references/trimmomatic.jar' 
truseq3 = '../references/TruSeq3-SE.fa'


rule all:
	input:
		expand('samples/male_mix_{type}_{genotype}_M_fastqc.zip', genotype=genotype, type=sampletype),
		expand('samples/male_mix_{type}_{genotype}_M_trimmed.fastq.gz', genotype=genotype, type=sampletype),
		expand('samples/male_mix_{type}_{genotype}_M_trimmed_fastqc.zip', genotype=genotype, type=sampletype),
		multiqc_report = 'samples/multiqc_report.html'
rule fastqc:
	input:
		fastqgz = 'samples/male_mix_{sample}_{genotype}_M.fastq.gz'
	output:
		fastqc = 'samples/male_mix_{sample}_{genotype}_M_fastqc.zip'
	threads: 4
	shell:
		'fastqc -t {threads} {input.fastqgz}'

rule trimmomatic:
	input:
		fastqgz_zip = 'samples/male_mix_{sample}_{genotype}_M_fastqc.zip',
		fastqgz = 'samples/male_mix_{sample}_{genotype}_M.fastq.gz',
		jar = trimmomatic_jar,
		fa = truseq3
	output:
		fastqgz_trim = 'samples/male_mix_{sample}_{genotype}_M_trimmed.fastq.gz'
	log:
		'samples/male_mix_{sample}_{genotype}_M_trimmed.log'
	threads: 8
	shell:
		'java -jar {input.jar} SE'
		' -threads {threads}'
		' {input.fastqgz} {output.fastqgz_trim}'
		' ILLUMINACLIP:{input.fa}:2:30:10 LEADING:3 TRAILING:3'
		' CROP:50 SLIDINGWINDOW:4:15 MINLEN:50 2> {log}'

rule fastqc_trim:
	input:
		fastqgz_trim = 'samples/male_mix_{sample}_{genotype}_M_trimmed.fastq.gz'
	output:	
		fastqc_trim = 'samples/male_mix_{sample}_{genotype}_M_trimmed_fastqc.zip'
	threads: 4
	shell:
		'fastqc -t {threads} {input.fastqgz_trim}'

rule multiqc:
	input:
		expand('samples/male_mix_{sample}_{genotype}_M_trimmed_fastqc.zip', sample=sampletype, genotype=genotype),
		expand('samples/male_mix_{sample}_{genotype}_M_trimmed.fastq.gz', sample=sampletype, genotype=genotype),
		expand('samples/male_mix_{sample}_{genotype}_M_trimmed.bamstats.txt', sample=sampletype, genotype=genotype),
		expand('samples/male_mix_{sample}_{genotype}_M_trimmed_filtered.sorted.bamstats.txt', sample=sampletype, genotype=genotype)
	output:
		report = 'samples/multiqc_report.html'
	shell:
		'export LC_ALL=en_US.utf-8 && '
		'export LANG=$LC_ALL && '
		'multiqc -f /projectnb/perissilab/gps2ko_bat_chipseq/ '
		'--outdir /projectnb/perissilab/gps2ko_bat_chipseq/ '
		'-e homer -e macs2 '
		'--fullnames ' 
